# UsersRestful

## Overview

Simple servicio REST con Base de Datos [H2](https://www.h2database.com/html/main.html) incrustada y pre-iniciada con datos inciales guardados.

Hace las operaciones básica de CRUD para un tipo usuario que tiene 3 miembros: ``name``, ``surname`` y ``details``. Tan solo ``details`` es opcional.

## Getting started

Arranque la aplicación como una aplicación de Spring Boot o utilize el fichero ``UsersApplication.launch`` en Eclipse que arranca la clase ``Main``.

## Demo

En la carpeta raíz se en capturas de Postman se muestran las operaciones de éxito que se pueden hacer de ejemplo.
