package users.controller;

import java.util.List;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

import users.dto.UserDto;
import users.model.User;
import users.service.UsersService;

@RestController
@RequestMapping("/users")
public class UsersController {

	@Autowired
	private UsersService service;

	@GetMapping()
	public ResponseEntity<List<User>> listarTodos() {
		return ResponseEntity.ok(service.obtenerTodos());
	}

	@GetMapping("/{id}")
	public ResponseEntity<UserDto> listarPorId(@PathVariable @NotNull Long id) {
		UserDto userDto = service.obtenerPorId(id);
		return ResponseEntity.ok(userDto);
	}

	@PostMapping()
	public ResponseEntity<@Valid UserDto> crearUser(@RequestBody @Valid UserDto user, UriComponentsBuilder uriBuilder) {
		service.crearUser(user);
		return ResponseEntity.ok(user);
	}

	@PutMapping("/{id}/details")
	public ResponseEntity<UserDto> atualizaStatus(@PathVariable Long id, @RequestBody String details) {
		UserDto user = service.actualizaDetails(id, details);
		return ResponseEntity.ok(user);
	}

	@DeleteMapping("/{id}")
	public ResponseEntity<UserDto> borraUser(@PathVariable Long id, @RequestBody String details) {
		service.borraUser(id);
		return ResponseEntity.ok().build();
	}

}
