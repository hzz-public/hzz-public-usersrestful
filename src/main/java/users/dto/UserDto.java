package users.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class UserDto {

	private Long id;
	private String name;
	private String surname;
	private String details;

	public Long getId() {
		return this.id;
	}

	public void setDetails(String details) {
		this.details = details;
	}

}
