package users.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.transaction.annotation.Transactional;

import users.model.User;

@Transactional
public interface UsersRepository extends JpaRepository<User, Long> {
}
