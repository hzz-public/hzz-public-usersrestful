package users.service;

import java.util.List;

import javax.persistence.EntityNotFoundException;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import lombok.RequiredArgsConstructor;
import users.dto.UserDto;
import users.model.User;
import users.repository.UsersRepository;

@Service
@RequiredArgsConstructor
public class UsersService {

	@Autowired
	private UsersRepository repository;

	private final ModelMapper modelMapper = new ModelMapper();

	public List<User> obtenerTodos() {
		return repository.findAll();
	}

	public UserDto obtenerPorId(Long id) {
		User user = repository.findById(id).orElseThrow(EntityNotFoundException::new);
		return modelMapper.map(user, UserDto.class);
	}

	public User obtenerEntityPorId(Long id) {
		User user = repository.findById(id).orElseThrow(EntityNotFoundException::new);
		return user;
	}

	public UserDto crearUser(UserDto userDto) {
		User user = modelMapper.map(userDto, User.class);
		user = repository.saveAndFlush(user);

		return modelMapper.map(user, UserDto.class);
	}

	public UserDto actualizaDetails(Long id, String details) {
		User user = obtenerEntityPorId(id);
		if (user == null) {
			throw new EntityNotFoundException();
		}

		user.setDetails(details);
		repository.saveAndFlush(user);

		return modelMapper.map(user, UserDto.class);
	}

	public void borraUser(Long id) {
		repository.deleteById(id);
	}
}
